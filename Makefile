get-harbor-admin-password:
	@ kubectl get secret -n harbor harbor-core-envvars -o jsonpath='{.data.HARBOR_ADMIN_PASSWORD}' | base64 -D; echo

reconcile:
	flux reconcile source git flux-system