# open a shell with all the tools

`nix develop`

# validate

`./scripts/validate.sh`

# get harbor admin password

`get/harbor-admin-password`