# Restarting the deployment via a service account

A role binding and service account is created.

Get a token for the service account with

```shell
kubectl create token -n flow deployment-restarter
```

Get the server cs from a working `.kube/config`

Then use the following to restart the deployment via the `deployment-restarter` service account

```shell
kubeconfigData=$(cat<<DATA>kubeconfig
apiVersion: v1
kind: Config
preferences: {}
current-context: service-account-context
clusters:
  - name: service-account-cluster
    cluster:
      server: $address
      certificate-authority-data: $serverCa
contexts:
  - name: service-account-context
    context:
      cluster: service-account-cluster
      user: service-account-user
users:
  - name: service-account-user
    user:
      token: $token
DATA
)

export KUBECONFIG=$(pwd)/kubeconfig:service-account-context
kubectl rollout restart deployment -n flow flow-deployment
```