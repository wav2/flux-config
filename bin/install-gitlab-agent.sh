#helm repo add gitlab https://charts.gitlab.io
#helm repo update
helm upgrade --install test gitlab/gitlab-agent \
    --namespace gitlab-agent-test \
    --create-namespace \
    --set image.tag=<current agentk version> \
    --set config.token=<your_token> \
    --set config.kasAddress=<address_to_GitLab_KAS_instance>