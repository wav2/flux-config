### Bootstrap flux

```shell
kubectl create ns cert-manager
kubectl create ns external-dns
kubectl create ns flux-system
kubectl create namespace sonarqube
kubectl create namespace harbor

cat ~/.config/age/age.agekey |
kubectl create secret generic sops-age \
  --namespace=flux-system \
  --from-file=age.agekey=/dev/stdin
```

#### for dev

```shell
# domain is 'git.dev.wav.im', but will get from hostname 'dev'
# ConfigMap for Corefile from CoreDNS contains 'dev' so it will resolve to host IP
flux bootstrap git --url=ssh://forgejo@git.dev.wav.im:2222/wav/flux-config.git --private-key-file=$HOME/.ssh/id_ed25519 --path=clusters/dev
```

#### for nix

```shell
# domain is 'git.wav.im', but will get from hostname 'nix'
# ConfigMap for Corefile from CoreDNS contains 'nix' so it will resolve to host IP
flux bootstrap git --url=ssh://forgejo@git.wav.im:2222/wav/flux-config.git --private-key-file=$HOME/.ssh/id_ed25519 --path=clusters/nix
```

### Setup CodeDNS for IPv6

```shell
kubectl -n kube-system edit configmap coredns
```

And change the lines in the "Corefile" and the "NodeHosts"

```
  Corefile: |
    .:53 {

        forward . 2001:4860:4860::8888 2001:4860:4860::8844
        # forward . /etc/resolv.conf
        
    }
  NodeHosts: |
    2001:8004:27b8:4b3d:44a7:ac79:ba2a:5aa dev
    2001:8004:27b8:4b3d:44a7:ac79:ba2a:5aa git.dev.wav.im  
```


