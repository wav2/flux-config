# Preventing data loss

A PV will not be deleted when it has the `pv-protection` attribute. Deleting this attribute allows deletion to finish.

```yaml
kind: PersistentVolume
metadata:
  finalizers: [kubernetes.io/pv-protection]
# ...
```

When a PersistentVolumes has a reclaim policy set to `Retain`, when the claim is deleted, the volume remains.

It transitions from `Bound` to `Released`. To transition it to `Available` so it can be `Bound` again, 
edit the PersistentVolume and delete the `spec/claimRef` attribute.

```yaml
kind: PersistentVolume
spec:
    persistentVolumeReclaimPolicy: Retain
# ...
```

For StatefulSets that create their volumes dynamically, a `persistentVolumeClaimRetentionPolicy` can be configured.

```yaml
kind: StatefulSet
spec:
  persistentVolumeClaimRetentionPolicy:
    whenDeleted: Retain
    whenScaled: Retain
# ...
```

A helm chart can specify that a resource shouldn't be uninstalled using the `helm.sh/resource-policy` annotation

```yaml
kind: Secret
metadata:
  annotations:
    "helm.sh/resource-policy": keep
# ...
```

A helm chart may have values that can refer to volumes that are created outside the lifecycle of a chart. Here,
you can create a PV and a PVC and then refer to it in the chart. The chart will use this PVC instead of dynamically
allocating its own.

```yaml
kind: HelmRelease
metadata:
  name: harbor
spec:
  values:
    persistence:
      persistentVolumeClaim:
        registry:
          existingClaim: harbor-registry-pvc
# ...
    postgresql:
      primary:
        persistence:
          existingClaim: data-harbor-postgresql-pvc
# ...
```