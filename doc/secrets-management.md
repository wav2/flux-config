# Create a secret file

```yaml
kubectl create secret generic cert-manager \
--namespace=cert-manager \
--from-file=do-token=../../raw_secrets/nix/digitalocean-token \
--dry-run=client \
-o yaml > cert-manager-do-token.yaml
```

# Encrypt the secret file

```yaml
sops --encrypt --in-place cert-manager-do-token.yaml
```

# Cluster setup

```shell
# Generate
age-keygen -o age.agekey
##> Public key: age1e...

# Upload
cat age.agekey |
kubectl create secret generic sops-age \
--namespace=flux-system \
--from-file=age.agekey=/dev/stdin
```

Then put the public key in the sops file

```
cat <<EOF > ./clusters/cluster0/.sops.yaml
creation_rules:
  - path_regex: .*.yaml
    encrypted_regex: ^(data|stringData)$
    age: age1e...
EOF```