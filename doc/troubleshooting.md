***Error, k8s API timeout***
`dial tcp 10.43.0.1:443: i/o timeout`

The API server is unavailable, check that k3s's externalIp is not set, as this is where it will be available internally, but idk.

Solution
- turn off the external ip for k3s
- wait till everything is working
- reapply external ip, for external dns

***Error, CertManager can't complete challenge***
`E0516 08:40:01.628178       1 sync.go:190] "cert-manager/challenges: propagation check failed" err="Could not determine authoritative nameservers for \"_acme-challenge.flow.wav.im.\"" resource_name="flow-cert-1-497577724-2335058171" resource_namespace="istio-system" resource_kind="Challenge" resource_version="v1" dnsName="flow.wav.im" type="DNS-01"`

Solution, DNS isn't resolved for some reason. May take some patience and the following Helm chart config.

```yaml
  values:
    installCRDs: true
    dns01RecursiveNameserversOnly: true
    dns01RecursiveNameservers: "8.8.8.8:53,1.1.1.1:53"
    logLevel: 4
```