# Now
- [ ] tekton
- [x] Secrets management: sops-nix
- [ ] Backup certificate data automatically
- [ ] Snapshots and backups
- [ ] group concepts visually
- [ ] flux resources compiled with nix lang - https://media.ccc.de/v/nixcon-2023-35290-nix-and-kubernetes-deployments-done-right#t=838

# Later

- [ ] Open telemetry
- [ ] Grafana login and wire services
- [ ] kluctl flux controller
- [ ] Deploy an app that uses NATS, CloudnativePG, makes, harbor, CI and nix cache
- [ ] Out of k8s cert management

# As needed

- [ ] sonarqube with persistent storage
- [ ] authelia (dex replacement, 2FA TOTP)
- [ ] dex idp: Microsoft login
  - limit access to clients
- [ ] SQL Server
- [ ] AutoGen + MemGPT
- [ ] put secrets in a different repository
- [ ] https://docs.renovatebot.com/configuration-options/

## Backlog
- [ ] Mattermost
- [ ] OpenProject
- [ ] ERPNext 
- [ ] Sign in with Apple
- [ ] Passkey
- [ ] Falco
- [ ] kubevirt - for windows?
- [ ] Secrets management: vault
- [ ] Vault: user store and management strategy (glauth ldap?)
- [ ] trust manager
- [ ] schema hero, liquibase
- [ ] ksniff
- [ ] Kafka
- [ ] Dapr
- [ ] wasmCloud
- [ ] Ansible
- [ ] CUE
- [ ] Weave UI
- [ ] nixery
- [ ] krr - Kubernetes Resource Recommendations
- [ ] Kubeapps
- [ ] Dynamically choose resource constraints (scale) at runtime based on user request for performance, load over time and traffic (own scheduler?).
- [ ] Use workspaces to limit blast radius: Cluster, Workspace, OPA Gatekeeper, start https://freshbrewed.science/2022/04/19/docker-with-tests3.html
- [ ] Tenants
- [ ] spiffe & spire (node attestation)
- [ ] route cluster traffic locally, so we can use local dev tools, like debugging: telepresence
  - devfile, devspace
- [ ] https://www.tkng.io/cni/weave/
- [ ] knative, func
- [ ] WOL
- [ ] Nvidia
- [ ] Sleeping kubelet (desktop)
- [ ] pihole or bind
- [ ] crossplane
- [ ] telepresence
- [ ] Use AWS dns for VM
- [ ] https://ballerina.io
- [ ] Sourcegraph
- [ ] wasmCloud
- [ ] spyf (SBOM generation), grype (CVE scanning), SLSA L3 (Non-falsifiable provenance), SPIFF/SPIRE (Workload id mgt)
- [ ] make secret decryption only available to a controller
- [ ] Portworx and other backup solutions
- [ ] Keptin (vs. Flagger)
- [ ] secure service entries with a DestinationRule that has a MUTUAL tls traffic policy
- [ ] Event-based (push) reconciliation (use HTTPS)
- [ ] Deploy to Azure or AWS
- [ ] Policies - https://kubernetes.io/docs/concepts/policy/#implementations-admission-control

# Buggy (evaluated, with limited success/reliability)



# Books

- [ ] Refactoring to Patterns

# Tenants

Talk: [GitOpsConEU: flux gatekeeper helm](https://www.youtube.com/watch?v=agsnktpIxzU)

### What is a tenant? Can be any of:
- tenant per application
- tenant per env (prod, staging, dev)
- tenant per application per environment

### Fundamentals Independent Developer Platform (IDP)
- Provision buckets, queues and database.
- ... crossplane?

### Problems to solve with multi-tenancy:
- Teams making changes to another team's workload
- Team accesses secrets of another team
- Team doesn't specify resource constraints (mem/cpu) and consumes all resources (noisy neighbour)
- Team defines `Ingress` that intercepts another app's traffic
- Teams create their own NodePort/LoadBalancer services

### General problems:
- Deletion prevention
- Create groups

# Interesting

- https://deislabs.io
  - CNCF libraries
- dragon fly OCI registry (p2p) https://d7y.io
  - harbor alternative

# Done

- [x] istio
- [x] Gateway
- [x] External DNS (DO)
- [x] cert-manager
- [x] ServiceEntry -> VirtualService -> Gateway (git.wav.im)
- [x] FluxCD
- [x] Kustomize
- [x] Move config to `git.wav.im/wav/flux-config`
- [x] Kiali
- [x] OCI registry - ocir.wav.im
- [x] Event-based (push) reconciliation
- [x] Prometheus
- [x] Jaeger (tracing)
- [x] How to avoid data loss (when namespace is deleted and there are secrets and PVCs associated)
- [x] OCI registry - use
- [x] Secrets management: sops flux
- [x] Review pull reconciliation intervals
- [x] Review dependency order and project organisation
- [x] dex - service entry (hosted on nix)
- [x] minio - service entry for blob.wav.im & minio.wav.im (hosted on nix)
- [x] minio authenticates with dex (nix config)
- [x] CI - Tekton Operator
- [x] Nats
- [x] Postgres - CloudNativePG
- [x] Use ipv6 addresses for VM via. exitnode (failed)
- [x] EC2 and Azure gateways to nix and dev
- [x] Postgres backup
- [x] lldap - user management (hosted on nix)
- [x] dex backed by lldap (nix config)
- [x] minio backed by dex (nix config)
- [x] harbor OCI backed by dex (nix & helm config)
- [x] sonarqube backed by dex (nix & helm config)
- [x] lldap manage roles, role = group, eg. hydra_admin
- [x] minio oidc configured with envvars (nix config)
- [x] minio policy observes minio_admin and minio_user groups
- [x] gitea uses ldap for auth observes git_admin and git_user groups
- [x] Nix Hydra
- [x] nix microvms (https://github.com/astro/microvm.nix)
- [x] attic (nix cache) - https://discourse.nixos.org/t/introducing-attic-a-self-hostable-nix-binary-cache-server/24343