{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";
  };
  outputs = { self, nixpkgs, flake-utils}:
    flake-utils.lib.eachDefaultSystem
      (system:
        let
          pkgs = import nixpkgs { inherit system; };
        in
        with pkgs;
        {
          devShells.default = mkShell {
            shellHook = ''
              zsh
            '';
            buildInputs = [
              # tools
              git
              dig
              bat
              silver-searcher
              curl
              yq-go
              sops
              age
              pwgen

              # kubernetes
              kubectl
              kubernetes-helm
              krew
              istioctl
              k9s
              k3d
              fluxcd
              starboard
              trivy
              kubeconform
              kustomize
            ];
          };
        }
      );
}