```shell
curl -sL https://github.com/operator-framework/operator-lifecycle-manager/releases/download/v0.28.0/install.sh | \
  bash -s v0.28.0
  
kubectl create -f https://operatorhub.io/install/cloudnative-pg.yaml

kubectl create -f https://operatorhub.io/install/redis-operator.yaml
```