# Certis with the Istio Gateway

"The Certificate should be created in the same namespace as the istio-ingressgateway deployment"

```yaml
apiVersion: cert-manager.io/v1
kind: Certificate
metadata:
  name: ingress-cert
  namespace: istio-system
spec:
  secretName: ingress-cert
  commonName: my.example.com
  dnsNames:
  - my.example.com
    ...
```