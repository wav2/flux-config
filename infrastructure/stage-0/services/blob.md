# 1. Login as admin

Obtain the admin login from the secret and login via. the portal `https://minio.wav.im`

# 2. Configure the OIDC provider

https://goharbor.io/docs/1.10/administration/configure-authentication/oidc-auth/

```yaml
OIDC Provider Name: dex
OIDC Endpoint: https://auth.wav.im
OIDC Client ID: minio
OIDC Client Secret: ...
OIDC Scope: openid,profile,offline_access,email
```

Callback url in provider should be https://minio.wav.im/oauth_callback

