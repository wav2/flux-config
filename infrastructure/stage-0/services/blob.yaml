---
apiVersion: cert-manager.io/v1
kind: Certificate
metadata:
  name: blob-cert
  namespace: istio-system
  labels:
    app.kubernetes.io/part-of: blob
spec:
  secretName: blob-cert-secret
  commonName: blob.wav.im
  dnsNames:
    - minio.wav.im
    - blob.wav.im
  issuerRef:
    name: letsencrypt-prod
    kind: ClusterIssuer
    group: cert-manager.io
---
apiVersion: networking.istio.io/v1beta1
kind: ServiceEntry
metadata:
  name: blob-ip
  namespace: nix-host
  labels:
    app.kubernetes.io/part-of: blob
spec:
  hosts:
    - blob.wav.im
  addresses:
    - 192.192.192.4/32 # Virtual IP
  ports:
    - number: 9000
      name: http
      protocol: HTTP
  location: MESH_EXTERNAL
  resolution: STATIC
  endpoints:
    - address: 10.42.0.1 # cni0 = 10.42.0.1 from command 'ip a' on host
---
apiVersion: networking.istio.io/v1beta1
kind: Gateway
metadata:
  name: blob-gateway
  namespace: nix-host
  labels:
    app.kubernetes.io/part-of: blob
spec:
  selector:
    istio: gateway
  servers:
    - port:
        number: 80
        name: http
        protocol: HTTP
      hosts:
        - blob.wav.im
      tls:
        httpsRedirect: true
    - port:
        number: 443
        name: https
        protocol: HTTPS
      tls:
        mode: SIMPLE
        credentialName: blob-cert-secret # This should match the Certificate secretName in the istio-system namespace
      hosts:
        - blob.wav.im
---
apiVersion: networking.istio.io/v1beta1
kind: VirtualService
metadata:
  name: blob-vs
  namespace: nix-host
  labels:
    app.kubernetes.io/part-of: blob
spec:
  hosts:
    - "blob.wav.im"
  gateways:
    - blob-gateway
  http:
    - route:
        - destination:
            host: blob.wav.im # to ServiceEntry
            port:
              number: 9000
---
apiVersion: networking.istio.io/v1beta1
kind: ServiceEntry
metadata:
  name: minio-ip
  namespace: nix-host
  labels:
    app.kubernetes.io/part-of: blob
spec:
  hosts:
    - minio.wav.im
  addresses:
    - 192.192.192.6/32 # Virtual IP
  ports:
    - number: 9001
      name: http
      protocol: HTTP
  location: MESH_EXTERNAL
  resolution: STATIC
  endpoints:
    - address: 10.42.0.1 # cni0 = 10.42.0.1 from command 'ip a' on host
---
apiVersion: networking.istio.io/v1beta1
kind: Gateway
metadata:
  name: minio-gateway
  namespace: nix-host
  labels:
    app.kubernetes.io/part-of: blob
spec:
  selector:
    istio: gateway
  servers:
    - port:
        number: 80
        name: http
        protocol: HTTP
      hosts:
        - minio.wav.im
      tls:
        httpsRedirect: true
    - port:
        number: 443
        name: https
        protocol: HTTPS
      tls:
        mode: SIMPLE
        credentialName: blob-cert-secret # This should match the Certificate secretName in the istio-system namespace
      hosts:
        - minio.wav.im
---
apiVersion: networking.istio.io/v1beta1
kind: VirtualService
metadata:
  name: minio-vs
  namespace: nix-host
  labels:
    app.kubernetes.io/part-of: blob
spec:
  hosts:
    - "minio.wav.im"
  gateways:
    - minio-gateway
  http:
    - route:
        - destination:
            host: minio.wav.im # to ServiceEntry
            port:
              number: 9001
---