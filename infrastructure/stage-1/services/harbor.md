# 1. Login as admin

Obtain the admin login from the secret and login via. the portal `https://ocir.wav.im`

# 2. Configure the OIDC provider

https://goharbor.io/docs/1.10/administration/configure-authentication/oidc-auth/

```yaml
OIDC Provider Name: dex
OIDC Endpoint: https://auth.wav.im
OIDC Client ID: harbor
OIDC Client Secret: ...
OIDC Scope: openid,profile,offline_access,email
```

Callback url in provider should be https://ocir.wav.im/c/oidc/callback



```secrets
oidc_name: dex
oidc_endpoint: https://auth.wav.im
oidc_client_id: harbor
oidc_client_secret: ...
oidc_scope: openid,profile,offline_access,email
```

```env
oidc_name
oidc_endpoint
oidc_client_id
oidc_client_secret
oidc_verify_cert
oidc_admin_group
oidc_groups_claim
oidc_group_filter
oidc_auto_onboard
oidc_extra_redirect_parms
oidc_scope
oidc_user_claim
```