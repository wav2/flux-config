## cert-manager

```shell
echo > cert-manager-secrets.yaml

kubectl create secret generic external-dns-aws \
--namespace cert-manager \
--from-file=aws_access_key_id=../../raw_secrets/dev/external-dns-aws-key-id \
--from-file=aws_secret_access_key=../../raw_secrets/dev/external-dns-aws-key \
--dry-run=client \
-o yaml >> cert-manager-secrets.yaml

echo "---" >> cert-manager-secrets.yaml

kubectl create secret generic letsencrypt-staging-issuer-account-key \
--namespace cert-manager \
--from-file=tls.key=../../raw_secrets/dev/letsencrypt-staging-issuer-account-key/tls.key \
--dry-run=client \
-o yaml >> cert-manager-secrets.yaml

echo "---" >> cert-manager-secrets.yaml

kubectl create secret generic letsencrypt-prod-issuer-account-key \
--namespace cert-manager \
--from-file=tls.key=../../raw_secrets/dev/letsencrypt-prod-issuer-account-key/tls.key \
--dry-run=client \
-o yaml >> cert-manager-secrets.yaml

sops --encrypt --in-place cert-manager-secrets.yaml
```
## external-dns

```shell
echo > external-dns-secrets.yaml

kubectl create secret generic external-dns-aws \
--namespace external-dns \
--from-file=credentials=../../raw_secrets/dev/external-dns-aws \
--dry-run=client \
-o yaml >> external-dns-secrets.yaml

sops --encrypt --in-place external-dns-secrets.yaml
```

## flux-system

```shell
echo > flux-system-secrets.yaml

kubectl create secret generic receiver-token \
--namespace flux-system \
--from-file=token=../../raw_secrets/dev/receiver-token \
--dry-run=client \
-o yaml >> flux-system-secrets.yaml

sops --encrypt --in-place flux-system-secrets.yaml
```