## cert-manager

```shell
echo > cert-manager-secrets.yaml

kubectl create secret generic cert-manager-dns \
--namespace cert-manager \
--from-file=do-token=../../raw_secrets/nix/digitalocean-token \
--dry-run=client \
-o yaml >> cert-manager-secrets.yaml

echo "---" >> cert-manager-secrets.yaml

kubectl create secret generic letsencrypt-staging-issuer-account-key \
--namespace cert-manager \
--from-file=tls.key=../../raw_secrets/nix/letsencrypt-staging-issuer-account-key/tls.key \
--dry-run=client \
-o yaml >> cert-manager-secrets.yaml

echo "---" >> cert-manager-secrets.yaml

kubectl create secret generic letsencrypt-prod-issuer-account-key \
--namespace cert-manager \
--from-file=tls.key=../../raw_secrets/nix/letsencrypt-prod-issuer-account-key/tls.key \
--dry-run=client \
-o yaml >> cert-manager-secrets.yaml

sops --encrypt --in-place cert-manager-secrets.yaml
```
## external-dns

```shell
echo > external-dns-secrets.yaml

kubectl create secret generic external-dns \
--namespace external-dns \
--from-file=do-token=../../raw_secrets/nix/digitalocean-token \
--dry-run=client \
-o yaml >> external-dns-secrets.yaml

sops --encrypt --in-place external-dns-secrets.yaml
```

## flux-system

```shell
echo > flux-system-secrets.yaml

kubectl create secret generic receiver-token \
--namespace flux-system \
--from-file=token=../../raw_secrets/nix/receiver-token \
--dry-run=client \
-o yaml >> flux-system-secrets.yaml

sops --encrypt --in-place flux-system-secrets.yaml
```

## harbor

For oidc configuration

```shell
echo > harbor-secrets.yaml

kubectl create secret generic harbor-config \
--namespace harbor \
--from-file=overrides.json=../../raw_secrets/nix/harbor-config.json \
--dry-run=client \
-o yaml >> harbor-secrets.yaml

sops --encrypt --in-place harbor-secrets.yaml
```

## sonarqube

kubectl create secret generic my-secret-config --from-file=secret.properties

```shell
echo > sonarqube-secrets.yaml

kubectl create secret generic sonarqube-secret \
--namespace sonarqube \
--from-file=secret.properties=../../raw_secrets/nix/sonarqube-secret.properties \
--from-file=sonarqube-password=../../raw_secrets/nix/sonarqube-password \
--dry-run=client \
-o yaml >> sonarqube-secrets.yaml

echo "---" >> sonarqube-secrets.yaml

kubectl create secret generic sonarqube-envvars \
--namespace sonarqube \
--from-env-file=../../raw_secrets/nix/sonarqube-envvars.properties \
--dry-run=client \
-o yaml >> sonarqube-secrets.yaml

sops --encrypt --in-place sonarqube-secrets.yaml
```

# flow

```shell
kubectl create secret generic flow-envvars \
--namespace flow \
--from-env-file=../../raw_secrets/nix/flow-envvars.properties \
--dry-run=client \
-o yaml >> flow-secrets.yaml

sops --encrypt --in-place flow-secrets.yaml
```