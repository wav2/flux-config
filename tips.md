## create a service account token

`kubectl -n istio-system create token kiali`

## viewing logs for a helm release

`flux logs --kind=HelmRelease --name=sonarqube --namespace=sonarqube --follow`

## Reset the code dns cache

Update the deployment to reset the cache when a pod is holding on to old DNS entries, like an external ip

```
kubectl -n kube-system set env deployment.apps/coredns FOO="BAR"
kubectl -n kube-system set env deployment.apps/coredns FOO
```

## Change the ExternalIP address used by the istio-gateway LoadBalancer

This will trigger an External DNS update

```
typ: LoadBalancer
for: istio-system/istio-gateway
set: status/loadBalancer/ingress/ip = [ "1.2.3.4" ]
```

## Choosing the CIDR range

When routing traffic through a AWS VPC, find the details:

```
IPv6 CIDR (Network border group)
2406:da1c:f69:c300::/56 (ap-southeast-2)
```

This is what you can use to set:


## k3s fails to start

The bootstrap data (cluster CA certificates and such) are stored in the datastore, 
encrypted with the token as the key generation passphrase.

```log
msg="starting kubernetes: preparing server: failed to normalize server token; must be in format K10<CA-HASH>::<USERNAME>:<PASSWORD> or <PASSWORD>"

https://github.com/k3s-io/k3s/issues/5345
```

```shell
rm /var/lib/rancher/k3s/server/token
```


